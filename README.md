

This repository is to run Cucumber a program from your PC to test aztecexchange.com website
 
### How do I get set up? ###
Download Cucumber and ruby gems
Navigate to the folder where you have cucumber installed
Create a Features folder
Copy and paste the .[dot]Features files I have uploaded into the features folder
Next within the Features folder create another folder called step_definitions and copy paste the .[dot]rb files into this folder
Next within the features folder create another folder called suppory and copy and paste the .[dot]env file into this folder. Tghe env file sets up the environment to run the Cucumber features



To run the tests Go to ruby command line
navigate to the Cucumber directory where the features folder is located an Init the cucumber by typing Cucumber --init
i.e C:\ users\brian\aztecassignment\cucumber --init
Next to run the feature Navigate to the directory where the feature is located and the cucumber (followed by directory.feature)
ie C:\ users\brian\aztecassignment\cucumber ./features/corporations.feature
This will then run the features file

Thank you,
Brian Cooke