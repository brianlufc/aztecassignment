Given(/^I visit Aztec exchange web site$/) do
visit("http://www.aztecexchange.com")
end
When (/^I select corporations web page$/) do
find(:xpath,'div[1]/header/div[2]/div/div/div/div/div/a[2]').click
end
# find path for investors
When (/^I test to check if login has an xpath$/) do
if page.has_xpath?('div[1]/header/div[2]/div/div/div/a[2]').click
then print 'Found path for investors'
end
end
# check for class_id
When (/^I test to check if contact has an address field$/) do
find_field('FirstName').value
end
When (/^I test to check linkedin has url link$/) do
visit"https://www.linkedin.com/company/aztec-exchange"
end
When (/^I fill enter text in Message field$/) do
fill_in('Message', :with => 'Sample message to test if field acceptd text')
end

When(/^I test to select "([^"]*)"$/) do |arg1|
select arg1, :from=> 'select2-drop-mask'
end
#When(/^I test to select "([^"]*)"$/) do |arg1|
#select arg1, :from=> 'select2-chosen'
#end

Then (/^I select EDI-Einvoice$/) do
find(:xpath,'div[1]/header/div[2]/div/div/div/div/div/a[2]').click
end

