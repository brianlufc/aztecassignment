


require 'capybara/cucumber'
require 'selenium/webdriver'
require 'pdf/reader'
require 'capybara-screenshot/cucumber'
require 'ruby-debug-ide'

Capybara.register_driver :selenium_chrome do |app|

  prefs = {
      :download => {
          :prompt_for_download => false,
          :default_directory => './downloads'
      },
      :excludeSwitches => ['test-type', 'ignore-certificate-errors' ]
  }

  caps = Selenium::WebDriver::Remote::Capabilities.chrome
  caps['chromeOptions'] = {'prefs' => prefs}
  Capybara::Selenium::Driver.new(app, :browser => :chrome, :desired_capabilities => caps)
end

Capybara.register_driver :selenium_ie do |app|
  Capybara::Selenium::Driver.new(app, :browser => :ie)
end

Capybara.run_server = true
Capybara.default_driver = :selenium_chrome
Capybara.javascript_driver = :selenium_chrome
Capybara.default_wait_time = 30
