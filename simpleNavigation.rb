Scenario: Navigation test
Given (/^I visit Aztec EDI Einvoice web page$/) do
visit'https://www.aztecexchange.com/Services/EDI-Einvoice'
end

# in this scenario after the first when, 'And' could replace the When 
when (/^I select About us$/) do
visit('/About')
end
when (/^I select select Aztec logo$/) do
click_link('Logo')
end
when (/^I select Investors$/) do
visit('/Investors')
end
when (/^I select select Aztec logo$/) do
click_link('Logo')
when (/^I select suppliers$/) do 
visit('/Suppliersuppliers')
end
when (/^I select select Aztec logo$/) do
click_link('Logo')
when (/^I select login-in$/) do
visit('/Login/Login')
end
when (/^I select select Aztec logo$/) do
click_link('Logo')
Then (/^I exit web site$/) do
visit('https://www.google.com')
end