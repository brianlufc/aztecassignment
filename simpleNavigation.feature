Feature: Simple Navigation

A test to check the navigability of Aztec exchange website

Scenario: Navigation test
Given I visit Aztec EDI Einvoice web page
When I select About us
When I select select Aztec logo
When I select Investors
When I select select Aztec logo
When I select suppliers
When I select select Aztec logo
When I select login-in
When I select select Aztec logo
Then I exit web site
  