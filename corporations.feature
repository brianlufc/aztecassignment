Feature: Corporation test

Scenario Outline: While in corporations tab test that page has required buttons

Given I visit Aztec exchange web site
When I select corporations web page
When I test to check if login has an xpath
When I test to check if contact has an First Name field
when I test to check linkedin has URL link
When I fill enter text in Message field
When I test to select "<Select2-dropmask1>"
When I test to fill in "<Select2-chosen2>"
Then I select EDI-Einvoice

Examples:
| Select2-chosen1 | Select2-chosen2 |
| Mr			  | Albania         |
| Mrs             | Andorra         |
| Ms              | Belgium         | 
| Dr              | Italy           | 

